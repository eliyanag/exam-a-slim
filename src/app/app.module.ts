import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';



import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';

import { RouterModule } from '@angular/router';
import { ProductsService } from './products/products.service';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FurebasComponent } from './furebas/furebas.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    EditProductComponent,
    FurebasComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserModule,
    HttpModule,
    RouterModule.forRoot([
    {path: '', component: ProductsComponent},
     {path: 'products', component: ProductsComponent},
     {path: 'edit-product/:id', component: EditProductComponent },
     {path: 'furebas', component: FurebasComponent},
     {path: 'login', component:LoginComponent},
     {path: '**', component: NotFoundComponent}
   ])
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
