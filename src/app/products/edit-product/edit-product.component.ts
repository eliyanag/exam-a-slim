import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { ProductsService } from '../products.service';
//import { ParamMap } from '@angular/router';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  // Adding emitters
  @Output() addProduct:EventEmitter <any> = new EventEmitter <any>();
  @Output() addProductPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  id;
  product;
  service:ProductsService;
  //Form Builder
  editProduct = new FormGroup({
      name:new FormControl(),
      price:new FormControl(),
      id:new FormControl()
  });  

  constructor(service:ProductsService, private formBuilder:FormBuilder, private route: ActivatedRoute) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getProduct(this.id).subscribe(response=>{
        this.product = response.json();                                
      });      
    });
  }

  sendData(){
    this.addProduct.emit(this.editProduct.value.name);
    this.editProduct.value.id = this.id;
    
    this.service.updateProduct(this.editProduct.value).subscribe(
      response =>{              
        this.addProductPs.emit();
      }

    )
  }

	ngOnInit() {        	  
  }

}