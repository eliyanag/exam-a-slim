import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;
  productsKeys;

  constructor(private service:ProductsService) {
    service.getProducts().subscribe(response=>{
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    });
 }

 updateProduct(id){
            this.service.getProduct(id).subscribe(response=>{
              this.products = response.json();      
          }); 

        }  
  ngOnInit() {
  }

}

