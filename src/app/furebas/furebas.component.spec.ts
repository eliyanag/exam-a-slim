import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FurebasComponent } from './furebas.component';

describe('FurebasComponent', () => {
  let component: FurebasComponent;
  let fixture: ComponentFixture<FurebasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FurebasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FurebasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
