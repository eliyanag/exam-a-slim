// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
 url: 'http://localhost/angular/slim/',
   firebase: {
    apiKey: "AIzaSyD8HVaUQDMKBKh_tVS72_cZf4ua5ILbewY",
    authDomain: "exam-b4c51.firebaseapp.com",
    databaseURL: "https://exam-b4c51.firebaseio.com",
    projectId: "exam-b4c51",
    storageBucket: "exam-b4c51.appspot.com",
    messagingSenderId: "676061746935"
  }
};
